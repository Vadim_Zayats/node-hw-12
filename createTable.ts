import { sequelize } from "./db";
import { DataTypes } from "sequelize";

const Video = sequelize.define("video", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  views: {
    type: DataTypes.FLOAT,
  },
  category: {
    type: DataTypes.TEXT,
  },
});

(async () => {
  try {
    await Video.sync({ force: true });
    console.log("Table videos created");
  } catch (error) {
    console.error("Error creating table:", error);
  }
  process.exit(0);
})();
