import { sequelize } from "./db";
import { DataTypes } from "sequelize";
import { argv } from "process";

const args = argv.slice(2);
const page = parseInt(args[0]);
const size = parseInt(args[1]);

const Video = sequelize.define("video", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  views: {
    type: DataTypes.FLOAT,
  },
  category: {
    type: DataTypes.TEXT,
  },
});

(async () => {
  try {
    const videos = await Video.findAll({
      limit: size,
      offset: (page - 1) * size,
    });
    console.log("Videos:", videos);
  } catch (error) {
    console.error("Error fetching records:", error);
  }
  process.exit(0);
})();
