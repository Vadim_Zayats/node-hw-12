import { sequelize } from "./db";
import { DataTypes, Model, Sequelize } from "sequelize";

// Визначте інтерфейс Video
interface VideoInstance extends Model {
  id: number;
  title: string;
  views: number;
  category: string;
}

// Визначте модель Video
const Video = sequelize.define<VideoInstance>("video", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  views: {
    type: DataTypes.FLOAT,
  },
  category: {
    type: DataTypes.TEXT,
  },
});

(async () => {
  try {
    const results = await Video.findAll({
      attributes: [
        "category",
        [sequelize.fn("sum", sequelize.col("views")), "totalViews"],
      ],
      group: ["category"],
    });
    results.forEach((result: any) => {
      // Використовуйте "any", якщо не можете точно визначити тип
      console.log(`${result.category} - ${result.dataValues.totalViews}`);
    });
  } catch (error) {
    console.error("Error fetching records:", error);
  }
  process.exit(0);
})();
