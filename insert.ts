import { sequelize } from "./db";
import { DataTypes } from "sequelize";
import { argv } from "process";

// Отримання аргументів командного рядка
const args = argv.slice(2);
const title = args[0];
const views = parseFloat(args[1]);
const category = args[2];

// Визначення моделі
const Video = sequelize.define("video", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  views: {
    type: DataTypes.FLOAT,
  },
  category: {
    type: DataTypes.TEXT,
  },
});

// Додавання нового запису
(async () => {
  try {
    await Video.create({ title, views, category });
    console.log("New record inserted successfully");
  } catch (error) {
    console.error("Error inserting record:", error);
  }
  process.exit(0);
})();
