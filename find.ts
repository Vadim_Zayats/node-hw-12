import { sequelize } from "./db";
import { DataTypes, Op } from "sequelize";
import { argv } from "process";

const args = argv.slice(2);
const search = args[0];

const Video = sequelize.define("video", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  views: {
    type: DataTypes.FLOAT,
  },
  category: {
    type: DataTypes.TEXT,
  },
});

(async () => {
  try {
    const videos = await Video.findAll({
      where: {
        title: {
          [Op.like]: `%${search}%`,
        },
      },
    });
    console.log("Found videos:", videos);
  } catch (error) {
    console.error("Error fetching records:", error);
  }
  process.exit(0);
})();
